import QtQuick 2.14
import QtQuick.Window 2.14
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtWebEngine 1.8
import QtGraphicalEffects 1.12
import "controls"

ApplicationWindow {
    id: window;
    visible: true
    width: 1000
    height: 600
    title: qsTr("Renegade Web Browser")
    flags: Qt.Windows | Qt.FramelessWindowHint;

    property int previousX
    property int previousY

    opacity: 0.5


    menuBar : Rectangle {
        id:titleHeader
        width: parent.width;
        height: (!webEngineViewHasScrolled()) ? 30 : 0;
        radius: 5;
        color: "steelblue"//Qt.transparent;//webEngineViewer.backgroundColor;

        MouseArea {
            anchors.fill: parent;

            cursorShape: (pressedButtons==Qt.LeftButton)? Qt.ClosedHandCursor : Qt.ArrowCursor;
            onPressed: {
                window.previousX = mouseX;
                window.previousY = mouseY;
            }
            onMouseXChanged: {
                window.x = window.x + mouseX - window.previousX;
            }
            onMouseYChanged: {
                window.y = window.y + mouseY - window.previousY;
            }
        }
        ProgressBar {
            anchors.bottom: parent.bottom;
            anchors.left:  parent.left;
            anchors.right: parent.right;
            height: 2;

            visible: webEngineViewer.loading;
            from:   0;
            to:     100;
            value:  webEngineViewer.loadProgress;
            indeterminate: (webEngineViewer.loading && webEngineViewer.loadProgress==0) ;
        }


        BrowserControls {
            id: browserControls
            x: 16;
            y: 0;

            state: webEngineViewHasScrolled()?"vertical":"";
            hasNext: webEngineViewer.canGoForward;
            hasPrevious:webEngineViewer.canGoBack;
            webLogo: webEngineViewer.icon;

            onNextClicked: webEngineViewer.goForward();
            onBackClicked: webEngineViewer.goBack();
        }

        Label {
            id:title
            anchors.left: browserControls.right;
            anchors.leftMargin: 10;
            text: (webEngineViewer.loading) ? webEngineViewer.url : webEngineViewer.title ;
            color: "white";
            visible: webEngineViewer.scrollPosition.y===0;
            y: 5;
            font.family: "Gargi";
            font.pixelSize: 12;
        }

        WindowControls {
            anchors.right: parent.right;
            anchors.top: parent.top;

            anchors.rightMargin: webEngineViewHasScrolled()?25:5 ;
            anchors.topMargin: webEngineViewHasScrolled()?2:5 ;

            state: webEngineViewHasScrolled()?"minimized":"";
            rectangleColor: titleHeader.color;

            onCloseClicked: window.close();
            onMinimizeClicked: window.showMinimized();
            onMaximizeClicked: {
                if(window.visibility!=Qt.WindowMaximized) {
                    window.showNormal();
                } else {
                    window.showMaximized();
                }
            }

            NumberAnimation on anchors.rightMargin { duration: 1000; }
            NumberAnimation on anchors.topMargin { duration: 1000; }
        }
    }



    WebEngineView {
        id: webEngineViewer;
        anchors.fill: parent;
        anchors.bottomMargin: -30
        url: "https://qwant.fr";
        //url:"http://gattaca.myds.me/audio"
        //url:"http://192.168.1.102:5000"
        //url:"https://www.mailo.com"
        backgroundColor: Qt.transparent
        onNewViewRequested: function(request) {
            webEngineViewer.url = request.requestedUrl;
        }

    }
    MouseArea {
        anchors.fill: parent
        propagateComposedEvents: true
        onClicked: {
            console.log("YOupi")
            mouse.accepted = false;
        }

        onPressed:  {
            console.log("YOupi")
            mouse.accepted = false;
        }
        onReleased: mouse.accepted = false;
        onDoubleClicked: mouse.accepted = false;
        onPositionChanged: mouse.accepted = false;
        onPressAndHold: mouse.accepted = false;
    }


//    Rectangle {
//        id: header
//        anchors {
//            left: parent.left;
//            right: parent.right;
//            top: parent.top;
//        }
//        height: 60
//        color: Qt.white
//    }
    Item {
        id: header
        x:0
        y:0
        width: parent.width
        height: 60;
         ShaderEffectSource {
           id: effectSource
           sourceItem: webEngineViewer
           anchors.fill: parent
           sourceRect: Qt.rect(0,0,parent.width,60)
         }
         FastBlur{
           id: blur
           anchors.fill: effectSource
           source: effectSource
           radius: 48
         }
         Rectangle {
             anchors.fill: parent
             opacity: 0.25
             color: "steelblue"
         }

    }
    DropShadow {
        anchors.fill: header
        horizontalOffset: 3
        verticalOffset: 3
        radius: 8.0
        samples: 17
        color: "#80000000"
        source: header
    }

/* Clone Browser View...
    ShaderEffectSource {
      id: effectSource2
      sourceItem: webEngineViewer
      x:100;
      y:100;
      height: 300;
      width: 300;
      live: true;
      //sourceRect: Qt.rect(0,0,parent.width,60)
    }

    DropShadow {
        anchors.fill: effectSource2
        horizontalOffset: 3
        verticalOffset: 3
        radius: 8.0
        samples: 17
        color: "#80000000"
        source: effectSource2
    }
*/


    function webEngineViewHasScrolled()
    {
        return webEngineViewer.scrollPosition.y !== 0;
    }

}
