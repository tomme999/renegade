import QtQuick 2.4

BrowserControlButtonForm {
    imageButton {
        source: (mouseArea.containsMouse)?imageHover:defaultImage;
    }

    mouseArea {
        onClicked: clicked();
    }
}
