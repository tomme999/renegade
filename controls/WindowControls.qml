import QtQuick 2.4

WindowControlsForm {
    butMinimize {
        onClicked:  minimizeClicked();
    }
    butMaximize {
        onClicked: maximizeClicked();
    }
    butClose {
        onClicked: closeClicked();
    }
    transitions: [
        Transition {
            NumberAnimation {
                properties: "rotation";
                duration: 200;
            }
            NumberAnimation {
                properties: "buttonSize";
                duration: 200;
            }
        }
    ]
}
