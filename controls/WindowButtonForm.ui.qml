import QtQuick 2.4

Item {
    id: element
    width: 100
    height: 100

    property string buttonDarkColor: "#00af00"
    property string buttonLightColor: "#00fa00"

    property alias mouseArea: mouseArea

    Rectangle {
        id: rectangle

        anchors.fill: parent

        gradient: Gradient {
            GradientStop {
                id: gradientStop
                position: 0.00
                color: (rectangle.hovered) ? "#00fa00" : "#000000"
            }
            GradientStop {
                position: 1.00
                color: buttonDarkColor
            }
        }
        color: "#ffffff"

        radius: height / 4.0

        MouseArea {
            id: mouseArea
            anchors.fill: parent
            hoverEnabled: true
        }
    }

    state: (mouseArea.containsMouse) ? "hover" : "normal"

    states: [
        State {
            name: "hover"
            PropertyChanges {
                target: gradientStop
                color: buttonLightColor
            }
        },
        State {
            name: "normal"
            PropertyChanges {
                target: gradientStop
                color: "#000000"
            }
        }
    ]
}
