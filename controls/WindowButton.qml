import QtQuick 2.4

WindowButtonForm {
    signal clicked();
    mouseArea {
        onClicked: clicked();
    }
}
