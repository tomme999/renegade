import QtQuick 2.4

Item {
    id: element

    height:24
    transformOrigin: Item.Left

    width: row.width

    property alias webLogo: imgLogo.source;
    property alias image3Visible: imgReload.visible
    property alias hasNext: browserControlButtonNext.visible
    property alias hasPrevious: browserControlButtonPrevious.visible

    property alias browserControlButtonPrevious: browserControlButtonPrevious;
    property alias browserControlButtonNext: browserControlButtonNext;


    signal backClicked
    signal nextClicked
    signal stopClicked
    signal reloadClicked

    Row {
        id: row
        x: 0
        y: 0
        spacing: 2
        rotation: 0
        transformOrigin: Item.Left

        Image {
            id: imgLogo
            width: 16
            height: 16
            anchors.verticalCenter: parent.verticalCenter
            source: "qrc:/qtquickplugin/images/template_image.png"
            fillMode: Image.PreserveAspectFit
            rotation: -element.rotation
        }

        BrowserControlButton {
            id: browserControlButtonPrevious
            width: 24
            height: 24
            defaultImage: "qrc:/Previous.svg"
            imageHover: "qrc:/PreviousSelected.svg"
            rotation: -element.rotation

        }
        BrowserControlButton {
            id: browserControlButtonNext
            width: 24
            height: 24
            defaultImage: "qrc:/Next.svg"
            imageHover: "qrc:/NextSelected.svg"
            rotation: -element.rotation

        }


        Image {
            id: imgReload
            width: 24
            height: 24
            source: "qrc:/qtquickplugin/images/template_image.png"
            fillMode: Image.PreserveAspectFit
            rotation: -row.rotation
        }

    }
    states: [
        State {
            name: "vertical"

            PropertyChanges {
                target: element
                rotation: 90
            }

            PropertyChanges {
                target: imgLogo
                height: 24
                opacity: 0.5
            }

            PropertyChanges {
                target: browserControlButtonPrevious
                opacity: 0.5
            }

            PropertyChanges {
                target: browserControlButtonNext
                opacity: 0.5
            }

            PropertyChanges {
                target: imgReload
                opacity: 0.5
            }
        }
    ]
}
