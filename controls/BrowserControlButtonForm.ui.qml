import QtQuick 2.4
import QtGraphicalEffects 1.0

Item {
    width: 400
    height: 400

    property string imageHover: "qrc:/NextSelected.svg"
    property string defaultImage: "qrc:/Next.svg"

    property alias imageButton: imageButton
    property alias mouseArea: mouseArea

    signal clicked;

    Image {
        id: imageButton
        x: 0
        y: 0
        width: 24
        height: 24
        source: defaultImage
        fillMode: Image.PreserveAspectFit

        opacity: 1

        MouseArea {
            id: mouseArea
            anchors.fill: parent
            hoverEnabled: true
        }
    }
}
