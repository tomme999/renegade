import QtQuick 2.4

Item {
    id: main

    property int buttonSize: 15

    property alias  butMinimize: butMinimize;
    property alias  butMaximize: butMaximize;
    property alias  butClose: butClose;
    property alias rectangleColor: rectangle.color

    signal minimizeClicked
    signal maximizeClicked
    signal closeClicked

    transformOrigin: Item.Right

    height: row.height

    width: row.width

    Rectangle {
        id: rectangle
        color: "#ffffff"
        anchors.top: row.top
        anchors.right: row.right
        anchors.bottom: row.bottom
        anchors.left: row.left
        anchors.leftMargin: -2
        anchors.topMargin: -2
        anchors.rightMargin: -2
        anchors.bottomMargin: -2

        opacity: 50
    }

    Row {
        id: row

        spacing: 5

        WindowButton {
            id: butMinimize
            height: main.buttonSize
            width: main.buttonSize
            rotation: -main.rotation
        }

        WindowButton {
            id: butMaximize
            height: main.buttonSize
            width: main.buttonSize
            buttonDarkColor: "#afaf00"
            buttonLightColor: "#fafa00"
            rotation: -main.rotation
        }

        WindowButton {
            id: butClose
            height: main.buttonSize
            width: main.buttonSize

            buttonDarkColor: "#af0000"
            buttonLightColor: "#fa0000"
            rotation: -main.rotation
        }
    }
    states: [
        State {
            name: "minimized"

            PropertyChanges {
                target: row
                spacing: 2
                rotation: 0
                transformOrigin: Item.Right
            }

            PropertyChanges {
                target: main
                rotation: -90
                transformOrigin: Item.Right
            }

            PropertyChanges {
                target: main
                buttonSize: 10
                transformOrigin: Item.Right
            }
        }
    ]
}
