import QtQuick 2.4

BrowserControlsForm {
    transitions: [
        Transition {
            NumberAnimation {
                properties: "rotation";
                duration: 200;
            }
            NumberAnimation {
                properties: "buttonSize";
                duration: 200;
            }
        }
    ]
    browserControlButtonPrevious {
        onClicked: backClicked();
    }
    browserControlButtonNext {
        onClicked: nextClicked();
    }
}
