import QtQuick 2.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Item {
    id: main;
height: 50;
width: 50;
    Rectangle {
        anchors.left: butMinimizedScreen.left;
        anchors.top: butClose.top;
        anchors.bottom: butMinimizedScreen.bottom;
        anchors.right: butClose.right;
        anchors.margins: -2 ;

        color: "#FF0000";
    }

    RowLayout {
        anchors.fill: parent
        spacing: 1

        RoundButton {
            id: butMinimizedScreen

            height: 15;
            width: 15 ;


            background: Rectangle {
                gradient: Gradient {
                    GradientStop {
                        position: 0.00;
                        color: (butMinimizedScreen.hovered)? "#00fa00": "#000000";
                    }
                    GradientStop {
                        position: 1.00;
                        color: "#00af00";
                    }
                }
                radius: width/4;
            }

            rotation: - main.rotation
        }

        RoundButton {
            id: butfullScreen

            height: 15;
            width: 15 ;


            background: Rectangle {
                color: transparent
                gradient: Gradient {
                    GradientStop {
                        position: 0.00;
                        color: (butfullScreen.hovered)?"#FaFa00": "#000000";
                    }
                    GradientStop {
                        position: 1.00;
                        color: "#afaf00";
                    }
                }
                radius: width/4;
            }
            rotation: - main.rotation

        }
        RoundButton {
            id: butClose

            height: 15;
            width: 15 ;
            background: Rectangle {
                color: transparent
                gradient: Gradient {
                    GradientStop {
                        position: 0.00;
                        color: butClose.hovered?"#fa0000": "#000000";
                    }
                    GradientStop {
                        position: 1.00;
                        color: "#af0000";
                    }
                }
                radius: width/4;
            }
            rotation: - main.rotation
        }
    }

    states: [
        State {
            name: "maximized"
            PropertyChanges {target: main; rotation: 0 }
        },
        State {
            name: "minimized"
            PropertyChanges {target: main; rotation: -90 }
        }
    ]
    transitions: [
        Transition {
            NumberAnimation {
                properties: "rotation";
                duration: 200;
            }
        }
    ]
}
